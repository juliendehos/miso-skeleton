{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module View where

import Common

import Data.Map (singleton, fromList)
import Data.Proxy (Proxy(..))
import Miso
import Prelude hiding (concat)
import Servant.API


clientViews :: (Model -> View Action) :<|> (Model -> View Action)
clientViews = homeView :<|> aboutView

viewModel :: Model -> View Action
viewModel m = 
    case runRoute (Proxy @ClientRoutes) clientViews modelUri m of
        Left _ -> text "not found"
        Right v -> v

homeView :: Model -> View Action
homeView m = div_ [class_ "container"]
    [ navDom
    , h1_ [] [ text "Home" ]
    , button_ [ class_ "btn btn-light", onClick FetchMessages ] [ text "Fetch messages" ]
    , button_ [ class_ "btn btn-light", onClick ResetMessages ] [ text "Reset messages" ]
    , div_ msgAttrs [ ul_ [] (map (\(Message c) -> li_ [] [text c]) (modelMessages m)) ]
    , footDom
    ]
    where msgAttrs = case modelMessages m of
            [] -> []
            _ -> [style_ $ fromList [ ("margin", "10px")
                                    , ("border", "1px solid grey")
                                    , ("border-radius", "10px")
                                    , ("padding-top", "10px")
                                    ]]

aboutView :: Model -> View Action
aboutView _ = div_ [class_ "container"]
    [ navDom
    , h1_ [] [ text "About" ]
    , p_ []
        [ text "This is a skeleton web app using "
        , a_ [href_ "https://haskell.org/"] [text "Haskell"]
        , text ", "
        , a_ [href_ "https://haskell-miso.org/"] [text "Miso"]
        , text " and "
        , a_ [href_ "https://www.servant.dev/"] [text "Servant"]
        , text "."
        ]
    , footDom
    ]

navDom :: View Action
navDom = nav_ [class_ "navbar navbar-expand"]
    [ div_ [style_ $ singleton "margin" "20px" ] [ text "Miso skeleton" ]
    , button_ [ class_ "btn btn-outline-primary", onClick $ ChangeUri homeRoute ] [ text "Home" ]
    , button_ [ class_ "btn btn-outline-primary", onClick $ ChangeUri aboutRoute ] [ text "About" ]
    ]

footDom :: View Action
footDom = div_ []
    [ hr_ []
    , a_ [href_ "https://gitlab.com/juliendehos/miso-skeleton"] [text "source code"]
    ]


