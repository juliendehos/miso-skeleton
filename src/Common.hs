{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module Common where

import Data.Aeson (FromJSON, ToJSON)
import Data.Proxy (Proxy(..))
import GHC.Generics (Generic)
import Miso
import Miso.String (concat, MisoString, ms)
import Prelude hiding (concat)
import Network.URI (URI)
import Servant.API
import Servant.Links


-- model

newtype Message = Message
    { messageContent :: MisoString
    } deriving (Eq, Generic)

instance FromJSON Message
instance ToJSON Message

data Model = Model 
    { modelMessages :: [Message]
    , modelUri :: URI
    } deriving (Eq)


-- actions

data Action
    = NoOp
    | ResetMessages
    | SetMessages [Message]
    | FetchMessages
    | SetUri URI
    | ChangeUri URI
    deriving (Eq)


-- client routes

type HomeRoute = View Action
type AboutRoute = "about" :> View Action
type ClientRoutes = HomeRoute :<|> AboutRoute

homeRoute :: URI
homeRoute = linkURI $ safeLink (Proxy @ClientRoutes) (Proxy @HomeRoute)

aboutRoute :: URI
aboutRoute = linkURI $ safeLink (Proxy @ClientRoutes) (Proxy @AboutRoute)


-- server routes 

type MessagesApi = "messages" :>  Get '[JSON] [Message]
type StaticApi = "static" :> Raw 

type PublicApi = MessagesApi :<|> StaticApi 

linkMessages :: URI
linkMessages = linkURI $ safeLink (Proxy @PublicApi) (Proxy @MessagesApi)

linkStatic :: URI
linkStatic = linkURI $ safeLink (Proxy @PublicApi) (Proxy @StaticApi)

mkStatic :: MisoString -> MisoString
mkStatic filename = concat [ms $ show linkStatic, "/", filename]


