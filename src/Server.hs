{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

import           Common
import           View

import           Data.Maybe (fromMaybe)
import           Data.Proxy (Proxy(..))
import qualified Lucid as L
import           Miso
import           Network.Wai.Handler.Warp (run)
import           Network.Wai.Middleware.RequestLogger (logStdout)
import           Servant
import           System.Environment (lookupEnv)


-- main program and data

main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    putStrLn $ "listening port " ++ show port ++ "..."
    run port $ logStdout $ serve (Proxy @ServerApi) server


-- server api and app

type ServerApi
    =    MessagesApi
    :<|> StaticApi
    :<|> ToServerRoutes ClientRoutes HtmlPage Action

messages :: [Message]
messages =
    [ Message "hello"
    , Message "world"
    ]

server :: Server ServerApi
server 
    =    pure messages
    :<|> serveDirectoryFileServer "static"
    :<|> (handleHome :<|> handleAbout)

handleHome :: Handler (HtmlPage (View Action))
handleHome = pure $ HtmlPage $ homeView $ Model [] homeRoute

handleAbout :: Handler (HtmlPage (View Action))
handleAbout = pure $ HtmlPage $ aboutView $ Model [] aboutRoute


-- view rendering

newtype HtmlPage a = HtmlPage a
    deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where
    toHtmlRaw = L.toHtml
    toHtml (HtmlPage x) = L.doctypehtml_ $ do
        L.head_ $ do
            L.meta_ [L.charset_ "utf-8"]
            L.meta_ [L.name_ "viewport", L.content_ "width=device-width, initial-scale=1, shrink-to-fit=no"]
            L.link_ [L.rel_ "stylesheet", L.href_ "static/bootstrap.4.1.3.min.css"]
            L.with 
                (L.script_ mempty) 
                [L.src_ (mkStatic "all.js"), L.async_ mempty, L.defer_ mempty] 
        L.body_ (L.toHtml x)

