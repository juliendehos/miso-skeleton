{-# LANGUAGE OverloadedStrings          #-}

import Common
import View

import Data.Aeson (decodeStrict)
import Data.Maybe (fromMaybe)
import JavaScript.Web.XMLHttpRequest
import Miso
import Miso.String (ms)

main :: IO ()
main = miso $ \ currentUri -> App 
    { initialAction = NoOp
    , model = Model [] currentUri
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = [ uriSub SetUri ]
    , mountPoint = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel ResetMessages m = noEff m { modelMessages = [] }
updateModel (SetMessages messages) m = noEff m { modelMessages = messages ++ modelMessages m }
updateModel FetchMessages m = m <# (SetMessages <$> xhrMessages)
updateModel (SetUri uri) m = noEff m { modelUri = uri }
updateModel (ChangeUri uri) m = m <# (pushURI uri >> pure NoOp)

xhrMessages :: IO [Message]
xhrMessages = 
    fromMaybe [] . decodeStrict . fromMaybe "" . contents <$> xhrByteString req
    where req = Request GET uri Nothing [] False NoData
          uri = ms $ show linkMessages

