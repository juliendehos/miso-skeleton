let
  pkgs = import ./nixpkgs.nix ;
in {
  inherit pkgs;
  server = pkgs.haskell.packages.ghc.callCabal2nix "app" ../. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "app" ../. {};
  miso-ghcjs = pkgs.haskell.packages.ghcjs.miso;
  miso-ghc = pkgs.haskell.packages.ghc.miso;
}

