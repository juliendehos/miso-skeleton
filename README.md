# miso-skeleton

a skeleton app using Haskell, Miso and Servant.

![](miso-skeleton.mp4)

## develop the project

- compile the client then compile and run the server:

```
make client && make run
```

## build a release

- build:

```
nix-build nix/release.nix
```

- test:

```
cd result
./bin/server
```

## build a docker image

- build:

```
nix-build nix/docker.nix
docker load < result
```

- test:

```
docker run --rm -it -p 3000:3000 app:latest
```

## deploy a docker image (manually)

- upload docker image to heroku:

```
heroku login
heroku container:login
heroku create miso-skeleton
docker tag app:latest registry.heroku.com/miso-skeleton/web
docker push registry.heroku.com/miso-skeleton/web
heroku container:release web --app miso-skeleton
```

- test the heroku app:

```
heroku logs
heroku open --app miso-skeleton
```

## deploy a docker image (using gitlab CI/CD)

- add a HEROKU_API_KEY variable in the Gitlab repo (see account setting in Heroku)

- create a container app in Heroku and add a APP_NAME variable in the Gitlab repo 

- uncomment the `production` job in `.gitlab-ci.yaml`

- push the master branch to update the app


## update cachix

- if `nix/nixpkgs.nix` has changed, update the cache on cachix:

```
nix-build nix/cache.nix | cachix push juliendehos
```

