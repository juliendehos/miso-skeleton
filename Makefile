client:
	nix-shell -A client --run "cabal --builddir=dist-client --config-file=config/client.config build client"
	mkdir -p static
	ln -sf ../`find dist-client/ -name all.js` static/

server:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config build server"

run:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config run server"

clean:
	rm -rf dist-* static/all.js 

